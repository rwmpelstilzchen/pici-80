function randomize_gender() {
	var hebrewGenders = ['fem', 'masc'];
	if (hebrewGenders[Math.floor(Math.random() * hebrewGenders.length)] == "fem") {
		femininize();
	}
	else {
		masculinize();
	}
}

function femininize() {
	jss.set('.f', {'display': 'inline'});
	jss.set('.m', {'display': 'none'});
	jss.set('#fbtn', {'border-bottom': '1px gray dotted',
		'padding-bottom': '2px',
		'margin-bottom': '-3px'});
	jss.set('#mbtn', {'border-bottom': 'none',
		'padding-bottom': '0px',
		'margin-bottom': '0px'});
}

function masculinize() {
	jss.set('.f', {'display': 'none'});
	jss.set('.m', {'display': 'inline'});
	jss.set('#fbtn', {'border-bottom': 'none',
		'padding-bottom': '0px',
		'margin-bottom': '0px'});
	jss.set('#mbtn', {'border-bottom': '1px gray dotted',
		'padding-bottom': '2px',
		'margin-bottom': '-3px'});
}
